<?php 
    require_once("../homepage/headerpage.php");
?>

<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">Form Transaksi</li>
</ol>

<div class="container">
    <div class="col-lg-6">
        <div class="page-header">
            <h3>Form Tambah Transaksi</h3>
        </div>
        <form action="../function/insert_trans.php" method="post" role="form" class="form-horizontal" include style="margin-left:1cm">
            <div class="form-group">
                <label>No.</label>
                <input type="text" name="id_transaksi" class="form-control" autofocus>
            </div>
            <div class="form-group">
                <label>Nama Pemesan</label>
                <input type="text" name="nama_pemesan" class="form-control">
            </div>
            <div class="form-group">
                <label>No. Hp</label>
                <input type="text" name="no_hp_pemesan" class="form-control">
            </div>
            <div class="form-group">
                <label>Tgl Berangkat</label>
                <input type="date" name="tgl_berangkat" class="form-control">
            </div>
            <div class="form-group">
                <label>Tgl kembali</label>
                <input type="date" name="tgl_kembali" class="form-control">
            </div>
            <div class="form-group">
                <label>Tujuan</label>
                <input type="text" name="tujuan" class="form-control">
            </div>
            <br>
            <div class="form-group" style="float:left">
                <input type="reset" value="Reset" class="btn btn-danger"/>
            </div>
            <div class="form-group" style="float:right">            
                <input type="submit" value="Submit" class="btn btn-primary" />
            </div>
    </div>
    </form>
</div>

<?php 
    require_once("../homepage/footerpage.php");
?>