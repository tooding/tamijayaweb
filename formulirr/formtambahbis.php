<?php 
    require_once("../homepage/headerpage.php");
?>

<div class="container" style="margin-left:1cm">
    <div class="col-lg-4">  
    <div class="page-header">
        <h3>Form Tambah BIS</h3>
    </div>
    
    <form action="../function/insert_bis.php" method="post" id="frmbis" role="form" class="form-horizontal" style="margin-left:1cm">
    <div class="form-group">
        <label>Nomor BIS</label>
            <input type="number" name="nomer_bis" class="form-control" id="nomer_bis">
    </div>
    <div class="form-group">
        <label>Nomor Plat BIS</label>
            <input type="text" name="no_pol_bis" class="form-control" id="no_pol_bis">
    </div>  
    <div class="form-group">
        <label>Harga Pokok</label>
            <input type="text" name="harga_pokok" class="form-control" id="harga_pokok">
    </div>
    <div class="form-group">
        <label>Status BIS</label>
            <select name="status_bis" id="status_bis" class="form-control inputstl">
                <option>Tersedia</option>
                <option>Tidak Tersedia</option>
                <option>Sedang Diperbaiki</option>
            </select>
      </div>
      <div class="form-group">
            <label>Jenis BIS</label>
                <select name="jenis_bis" id="jenis_bis" class="form-control inputstl">
                    <option>Mini Bus</option>
                    <option>Medium Bus</option>
                    <option>Big Bus</option>
                </select>
          </div>
          <div class="form-group">
            <label>Kapasitas</label>
            <select name="kapasitas_bis" id="kapasistas_bis" class="form-control inputstl">
                <option>30</option>
                <option>40</option>
                <option>60</option>
            </select>
        </div>
          <div class="form-group" style="float : right">                    
                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-primary">Tambahkan BIS</button>
               </div>
        </div> 
    </form>

    <div class="col-md-12">
            <nav align="center">
              <ul class="pagination">
                <li>
                  <a href="#" aria-label="Next">
                    <span aria-hidden="true">Next &raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>

<?php 
    require_once("../homepage/footerpage.php");
?>