<?php 
    require_once("../homepage/headerpage.php");
?>

<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../index/index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">Bis</li>
</ol>

<?php
include "../index/koneksi.php";
$select="SELECT * FROM bis";
$query=mysqli_query($kon,$select);
?>


   <!--Button Tambah Bis -->
  
 <div class="col-md-10" style="min-height:600px">
         <div class="col-md-12" style="padding:10px; padding-left:0;padding-right:0;">            
         <a href="formtambahbis.php" class="btn btn-info">Tambah Bis</a>
         </div>

            <table class="table table-bordered">
               <tr>
                  <th class="info">No. BIS</th>
                  <th class="info">No. Polisi</th>                                   
                  <th class="info">Harga Pokok</th>
                  <th class="info">Status</th>
                  <th class="info">Jenis</th>
                  <th class="info">Kapasitas</th>                   
                  <th class="info" colspan="2">Action</th>
               </tr>

<?php
while($row=mysqli_fetch_array($query)){
?>
<tr>
<td><?php echo $row['nomer_bis'];?></td>
<td><?php echo $row['no_pol_bis'];?></td>
<td><?php echo $row['harga_pokok'];?></td>
<td><?php echo $row['status_bis'];?></td>
<td><?php echo $row['jenis_bis'];?></td>
<td><?php echo $row['kapasitas_bis'];?></td>
<td>
<a href="formeditbis.php?nomer_bis=<?php echo $row['nomer_bis']; ?>">Edit</a>
</td>
<td>
<a href="../function/delete_bis.php?nomer_bis=<?php echo $row['nomer_bis']; ?>" onClick="return confirm('Apakah yakin data ingin dihapus ?')">Hapus</a>

</td>
</tr>

<?php
}
?>
               
            </table>
            <div class="col-md-12">
               <nav align="center">
                 <ul class="pagination">
                   <li>
                     <a href="#" aria-label="Previous">
                       <span aria-hidden="true">&laquo;</span>
                     </a>
                   </li>
                   <li><a href="#">1</a></li>
                   <li><a href="#">2</a></li>
                   <li><a href="#">3</a></li>
                   <li><a href="#">4</a></li>
                   <li><a href="#">5</a></li>
                   <li>
                     <a href="#" aria-label="Next">
                       <span aria-hidden="true">&raquo;</span>
                     </a>
                   </li>
                 </ul>
               </nav>

            </div>
   </div>


<?php 
    require_once("../homepage/footerpage.php");
?>