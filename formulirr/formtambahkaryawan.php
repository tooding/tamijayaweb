<?php 
    require_once("../homepage/headerpage.php");
?>

<?php
if (!empty($_SESSION['error'])) {
echo $_SESSION['error'];
unset($_SESSION['error']);
}
?>

<div class="container">
    <div class="col-lg-6">
        <div class="page-header">
            <h3>Form Tambah Karyawan</h3>
        </div>
        <form action="../function/insert_kary.php" method="post" role="form" class="form-horizontal" include style="margin-left:1cm">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" autofocus>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="passworduser" class="form-control">
            </div>
            <div class="form-group">
                <label>Nama Karyawan</label>
                <input type="text" name="namakaryawan" class="form-control">
            </div>
            <div class="form-group">
                <label>No Handphone Karyawan</label>
                <input type="text" name="NoHP" class="form-control">
            </div>
            <div class="form-group">
                <label>Alamat Karyawan</label>
                <textarea name="alamat" class="form-control"></textarea>
            </div><br>
            <div class="form-group">
                <label>Hak akses :</label><br>
                <input type="checkbox" name="input_transaksi" value="1">Menambahkan data Transaksi <br>
                <input type="checkbox" name="edit_transaksi" value="1">Mengubah data Transaksi <br>
                <input type="checkbox" name="delete_transaksi" value="1">Menghapus data Transaksi <br>
                <input type="checkbox" name="input_bis" value="1">Menambahkan data Bis <br>
                <input type="checkbox" name="edit_bis" value="1">Mengubah data Bis <br>
                <input type="checkbox" name="delete_bis" value="1">Menghapus data Bis <br>
                <input type="checkbox" name="input_karyawan" value="1">Menambahkan data Karyawan <br>
                <input type="checkbox" name="edit_karyawan" value="1">Mengubah data Karyawan <br>
                <input type="checkbox" name="delete_karyawan" value="1">Menghapus data Karyawan <br><br>
            </div>
            <div class="form-group" style="float:left">
                <input type="reset" value="Reset" class="btn btn-danger"/>
            </div>
            <div class="form-group" style="float:right">            
                <input type="submit" value="Submit" class="btn btn-primary" />
            </div>
    </div>
    </form>
</div>

<?php 
    require_once("../homepage/footerpage.php");
?>
