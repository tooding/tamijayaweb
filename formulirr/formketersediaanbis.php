<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css">
    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
    <script type='text/javascript'>
        $(function(){
        $('.input-daterange').datepicker({
            autoclose: true
        });
        });
        
        </script>
<?php 
    require_once("../homepage/headerpage.php");
?>

<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">Form Ketersediaan BIS</li>
</ol>
<div class="container">
    <div class="col-lg-6">
        <div class="page-header">
            <h3>Form Ketersediaan BIS</h3>
        </div>
        <form action="../function/ceksediabis.php" method="post" role="form" class="form-horizontal" include style="margin-left:1cm">
            <div class="form-group">
                <label>Jadwal Tanggal Pesanan</label>
                <div class="input-daterange input-group" id="datepicker">
                <span class="input-group-addon">Berangkat &nbsp;</span>
                    <input type="date" class="input-sm form-control" name="berangkat" />
                <span class="input-group-addon">&nbsp; Kembali &nbsp;</span>
                    <input type="date" class="input-sm form-control" name="kembali" />
            </div> 
            </div>
            <div class="form-group">
                <label>Kapasitas</label>
                <select name="kapasitas" id="kapasistas" class="form-control inputstl">
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
            </div>
           
            <br>
            <div class="form-group" style="float:left">
                <input type="reset" value="Reset" class="btn btn-danger"/>
            </div>
            <div class="form-group" style="float:right">            
                <input type="submit" value="Submit" class="btn btn-primary" />
            </div>
    </div>
    </form>
</div>

<?php 
    require_once("../homepage/footerpage.php");
?>