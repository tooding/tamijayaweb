<?php 
    require_once("../homepage/headerpage.php");
?>

<?php
include "../function/koneksi.php";
$select="SELECT * FROM transaksi ORDER BY id_transaksi DESC";
$query=mysqli_query($kon,$select);
?>

<div class="col-md-10" style="padding:0px">
<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../index/index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">Transaksi</li>
</ol>
   </div>
 <div class="col-md-10" style="min-height:600px">
         <div class="col-md-12" style="padding:10px; padding-left:0;padding-right:0;">            

            <a href="formtambahtransaksi.php" class="btn btn-info">Tambah Transaksi</a>

         </div>
            <table class="table table-bordered">
               <tr>
                
                  <th class="info">Nama Pemesan</th>
                  <th class="info">No. HP</th>
                  <!--<th class="info">Alamat</th>-->
                  <th class="info">Tgl Berangkat</th>
                  <th class="info">Tgl Kembali</th>
                  <!-- <th class="info">Tgl Kembali</th>
                  <th class="info">Alamat Jemput</th> -->
                  <th class="info">Tujuan</th>
                  <!--<th class="info">Harga</th>
                  <th class="info">Keterangan</th>
                  <th class="info">Petugas</th>-->
                  <th class="info" colspan="3">Action</th>
               </tr>               
<?php
while($row=mysqli_fetch_array($query)){
?>
<tr>

<td><?php echo $row['nama_pemesan'];?></td>
<td><?php echo $row['no_hp_pemesan'];?></td>
<td><?php echo $row['tgl_berangkat'];?></td>
<td><?php echo $row['tgl_kembali'];?></td>
<td><?php echo $row['tujuan'];?></td>
<td>
<a href="detil_trans.php?no_trans=<?php echo $row['id_transaksi']; ?>">Detail</a>
</td>
<td>
<a href="/tamijayaweb/function/delete_trans.php?no_trans=<?php echo $row['id_transaksi']; ?>" onClick="return confirm('Apakah yakin data ingin dihapus ?')">Hapus</a>

</td>
</tr>

<?php
}
?> 
               
            </table>
            <div class="col-md-12">
               <nav align="center">
                 <ul class="pagination">
                   <li>
                     <a href="#" aria-label="Previous">
                       <span aria-hidden="true">&laquo;</span>
                     </a>
                   </li>
                   <li><a href="#">1</a></li>
                   <li><a href="#">2</a></li>
                   <li><a href="#">3</a></li>
                   <li><a href="#">4</a></li>
                   <li><a href="#">5</a></li>
                   <li>
                     <a href="#" aria-label="Next">
                       <span aria-hidden="true">&raquo;</span>
                     </a>
                   </li>
                 </ul>
               </nav>

            </div>
   </div>

   <?php 
    require_once("../homepage/footerpage.php");
?>