-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2018 at 06:48 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamijaya`
--

-- --------------------------------------------------------

--
-- Table structure for table `bis`
--

CREATE TABLE `bis` (
  `nomer_bis` char(6) NOT NULL,
  `no_pol_bis` char(9) NOT NULL,
  `harga_pokok` int(8) NOT NULL,
  `status_bis` varchar(15) NOT NULL,
  `jenis_bis` varchar(8) NOT NULL,
  `kapasitas_bis` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bis`
--

INSERT INTO `bis` (`nomer_bis`, `no_pol_bis`, `harga_pokok`, `status_bis`, `jenis_bis`, `kapasitas_bis`) VALUES
('111112', 'AD1235AD', 6000, 'Tidak Tersedia', 'Medium B', 20),
('90', 'AB1234DFF', 700000, 'Tersedia', 'Big Bus', 60);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_transaksi` char(8) NOT NULL,
  `kd_dtl_transaksi` char(10) NOT NULL,
  `nomer_bis` char(6) NOT NULL,
  `nama_sopir` varchar(10) DEFAULT NULL,
  `nama_kondektur` varchar(10) DEFAULT NULL,
  `harga_jual` int(10) NOT NULL,
  `dp1` int(10) NOT NULL,
  `dp2` int(10) DEFAULT NULL,
  `dp3` int(10) DEFAULT NULL,
  `status` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `username` char(8) NOT NULL,
  `pasword` char(8) NOT NULL,
  `nama_karyawan` varchar(20) NOT NULL,
  `no_hp_karyawan` char(13) NOT NULL,
  `alamat_karyawan` varchar(50) NOT NULL,
  `input_transaksi` char(1) NOT NULL,
  `edit_transaksi` char(1) NOT NULL,
  `delete_transaksi` char(1) NOT NULL,
  `input_bis` char(1) NOT NULL,
  `edit_bis` char(1) NOT NULL,
  `delete_bis` char(1) NOT NULL,
  `input_karyawan` char(1) NOT NULL,
  `edit_karyawan` char(1) NOT NULL,
  `delete_karyawan` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`username`, `pasword`, `nama_karyawan`, `no_hp_karyawan`, `alamat_karyawan`, `input_transaksi`, `edit_transaksi`, `delete_transaksi`, `input_bis`, `edit_bis`, `delete_bis`, `input_karyawan`, `edit_karyawan`, `delete_karyawan`) VALUES
('', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('nina', 'nina123', 'Nina Wulandari', '12142434535', 'llhfshksf', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` char(8) NOT NULL,
  `nama_pemesan` varchar(20) NOT NULL,
  `no_hp_pemesan` char(13) NOT NULL,
  `alamat_pemesan` varchar(50) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tgl_berangkat` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `penjemputan` varchar(20) NOT NULL,
  `tujuan` varchar(20) NOT NULL,
  `harga_total` int(10) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `username` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bis`
--
ALTER TABLE `bis`
  ADD PRIMARY KEY (`nomer_bis`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`kd_dtl_transaksi`),
  ADD KEY `nomer_bis` (`nomer_bis`),
  ADD KEY `id_transaksi` (`id_transaksi`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `username` (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_1` FOREIGN KEY (`nomer_bis`) REFERENCES `bis` (`nomer_bis`),
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`username`) REFERENCES `karyawan` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
