<?php

require_once("koneksi.php");

$id_buku = $_POST["id_buku"];
$judul_buku = $_POST["judul_buku"];
$pengarang = $_POST["pengarang"];
$penerbit = $_POST["penerbit"];
$tahun_terbit = $_POST["tahun_terbit"];
$cover_buku = $_POST["cover_buku"];

$target_dir = "images/";
$namafile = uniqid().basename($_FILES["fileToUpload"]["name"]);
$target_file = $target_dir . $namafile;
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]); // tmp_name --> untuk tau file yang sudah di upload pada directori yang sudah disiapkan
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}



// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    $stmt = $conn->prepare("INSERT INTO buku(id_buku,judul_buku,pengarang,penerbit,tahun_terbit,cover_buku VALUES (?,?,?,?,?,?)");
    $stmt->bind_param("ssiiis", $id_buku,$judul_buku,$pengarang,$penerbit,$tahun_terbit,$cover_buku);
    try{
        $stmt->execute();
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
        $pesan = "buku $id_buku berhasil ditambahkan.";
        header("Location: /ukdwstore/tampilproduk.php?pesan=$pesan");
    }catch(Exception $e){
        $pesan = "Proses tambah Produk gagal, kesalahan:".$e->getMessage();
        header("Location: /ukdwstore/tambahproduk.php?pesan=$pesan");
    }
}

?>