<?php 
    /*if(!isset($_SESSION["username"])){
        header("Location: /ukdwstore/loginform.php");
    }*/
    require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Tambah Produk</li>
</ol>
<div class="row">
    <div class="col-6">
        <h1>Tambah Produk</h1>
        <form action="prosestambahproduk.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="id_buku">id buku:</label>
                <input type="text" class="form-control" name="idbuku">
            </div>
            <div class="form-group">
                <label for="judul_buku">judul_buku</label>
                <input type="text" class="form-control" name="judul_buku">
            </div>
            <div class="form-group">
                <label for="pengarang">pengarang</label>
                <input type="text" class="form-control" name="pengarang">
            </div>
            <div class="form-group">
                <label for="penerbit">penerbit</label>
                <input type="text" class="form-control" name="penerbit">
            </div>
            <div class="form-group">
                <label for="tahun_terbit">tahun_terbit</label>
                <input type="text" class="form-control" name="tahun_terbit">
            </div>
            <div class="form-group">
                <label for="fileToUpload">cover_buku</label>
                <input type="file" name="fileToUpload">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>

<?php 
    require_once("footerpage.php");
?>